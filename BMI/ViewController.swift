//
//  ViewController.swift
//  BMI
//
//  Created by iosdev on 5.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBAction func calculate(_ sender: UIButton) {
        let bmi = self.person.bmi()
        print(bmi)
        print(person.getHeight())
        bmiResult.text = String(bmi)
    }
    
    @IBOutlet weak var bmiResult: UILabel!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (component == 0) {
            return 700
        }
        return 270
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(row + 1)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (component == 0) {
            person.setWeight(Double(row + 1))
        } else {
            person.setHeight(Double(row + 1))
        }
    }
    
    var person = Person(
        name: "",
        age: 18,
        profession: [],
        weight: 0,
        height: 0
    )

    @IBOutlet weak var whPicker: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whPicker.delegate = self as UIPickerViewDelegate
        whPicker.dataSource = self as UIPickerViewDataSource
        // Do any additional setup after loading the view.
    }


}

