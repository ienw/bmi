//
//  Person.swift
//  BMI
//
//  Created by iosdev on 5.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation

class Person {
    var name: String
    private var age: Int
    private var profession: Array<String>
    private var weight: Double
    private var height: Double
    

    init (name:String, age:Int, profession:Array<String>,
    weight:Double, height:Double){
        self.name = name
        self.age = age
        self.profession = profession
        self.weight = weight
        self.height = height
    }
    
    func getAge() -> Int {
        return self.age
    }
    
    func setAge(_ newAge: Int) -> Int? {
        if(self.age > newAge && newAge < 0){
            return nil
        }
        self.age = newAge
        return self.age
    }
    
    func setWeight(_ newWeight: Double) -> Double? {
        if(newWeight < 0 && newWeight > 650){
            return nil
        }
        self.weight = newWeight
        return self.weight
    }
    
    func getWeight() -> Double {
        return self.weight
    }
    
    func setHeight(_ newHeight: Double) -> Double? {
        if(newHeight < 0 && newHeight > 280){
            return nil
        }
        self.height = newHeight
        return self.height
    }
    
    func getHeight() -> Double {
        return self.height
    }
    
    func addProfession(_ newProfession: String) -> Array<String>? {
        if(profession.count > 3) {
            return nil
        }
        self.profession.append(newProfession)
        return self.profession
    }
    
    func getProfessions() -> Array<String> {
        return self.profession
    }
    
    func bmi() -> Double {
        let heightMeter = height / 100
        return weight/(heightMeter*heightMeter)
    }
}

