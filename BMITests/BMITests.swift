//
//  BMITests.swift
//  BMITests
//
//  Created by iosdev on 5.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import XCTest
@testable import BMI

class BMITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testBMI() {
        let test = Person(name: "name", age: 22, profession: [], weight: 50.0, height: 160.0)
        let bmi = round(test.bmi() * 100) / 100
        XCTAssert(bmi == 19.53)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
